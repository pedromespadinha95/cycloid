package com.interview.cycloid.service;

import com.interview.cycloid.model.Channel;
import com.interview.cycloid.model.Program;

import java.util.List;
import java.util.Optional;

public interface ProgramService {

    List<Program> getProgramsByChannel(String channelId);
    Optional<Program> getProgram(String id);
    Program createProgram(Program program);
    void deleteProgram(String id);
}
