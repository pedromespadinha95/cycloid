package com.interview.cycloid.service.impl;

import com.interview.cycloid.model.Channel;
import com.interview.cycloid.repository.ChannelRepository;
import com.interview.cycloid.service.ChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChannelServiceImpl implements ChannelService {

    @Autowired
    private ChannelRepository channelRepository;

    @Override
    public List<Channel> getAllChannels() {
        return channelRepository.findAll();
    }

    public Channel createChannel(Channel channel){
        return channelRepository.save(channel);
    }

}
