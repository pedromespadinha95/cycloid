package com.interview.cycloid.service.impl;

import com.interview.cycloid.model.Channel;
import com.interview.cycloid.model.Program;
import com.interview.cycloid.repository.ChannelRepository;
import com.interview.cycloid.repository.ProgramRepository;
import com.interview.cycloid.service.ProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class ProgramServiceImpl implements ProgramService {

    @Autowired
    private ProgramRepository programRepository;

    @Autowired
    private ChannelRepository channelRepository;

    @Override
    public List<Program> getProgramsByChannel(String channelId) {
        return channelRepository.findById(channelId).map(ch -> ch.getPrograms()).orElseThrow(() -> null);
    }

    @Override
    public Optional<Program> getProgram(String id) {
        return programRepository.findById(id);
    }

    @Override
    public Program createProgram(Program program) {
        return programRepository.save(program);
    }

    @Override
    public void deleteProgram(String id) {
        programRepository.deleteById(id);
    }

}
