package com.interview.cycloid.service;

import com.interview.cycloid.model.Channel;

import java.util.List;

public interface ChannelService {

    List<Channel>  getAllChannels();
    Channel createChannel(Channel channel);
}
