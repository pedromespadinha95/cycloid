package com.interview.cycloid.repository;

import com.interview.cycloid.model.Program;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProgramRepository extends CrudRepository<Program, String> {
}
