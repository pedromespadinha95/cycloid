package com.interview.cycloid.model;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Channel implements Serializable {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ApiModelProperty(value = "Name", name = "name", dataType = "String", example = "RTP")
    @Column(name = "name", nullable = false)
    private String name;

    @ApiModelProperty(value = "Position on TV table", name = "position", dataType = "Integer", example = "1")
    @Column(name = "position", nullable = false)
    private Integer position;

    @ApiModelProperty(value = "Category of program tv", name = "category", dataType = "String", example = "Sports")
    @Column(name = "category", nullable = false)
    private String category;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "channel")
    @JsonManagedReference
    private List<Program> programs;
}