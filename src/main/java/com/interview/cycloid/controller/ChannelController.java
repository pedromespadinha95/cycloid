package com.interview.cycloid.controller;

import com.interview.cycloid.model.Channel;
import com.interview.cycloid.service.ChannelService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/channels")
public class ChannelController {

    @Autowired
    private ChannelService channelService;


    @ApiOperation(value = "Creates a new channel")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Channel created with sucess"),
            @ApiResponse(code = 500, message = "Exception"),
    })
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Channel createChannel(@RequestBody Channel channel){
        return channelService.createChannel(channel);
    }


    @ApiOperation(value = "List all channels")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Return all channel with success"),
            @ApiResponse(code = 500, message = "Exception"),
    })
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Channel> getAllChannels(){
        return channelService.getAllChannels();
    }
}
