package com.interview.cycloid.controller;

import com.interview.cycloid.model.Program;
import com.interview.cycloid.service.ProgramService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.constraints.NotBlank;
import java.util.List;

@RestController
@RequestMapping("/programs")
public class ProgramsController {

    @Autowired
    private ProgramService programService;

    @Autowired
    private ModelMapper modelMapper;

    @ApiOperation(value = "Create a new program")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Program created successfully"),
            @ApiResponse(code = 500, message = "Exception"),
    })
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Program createProgram(@RequestBody Program program){
        return programService.createProgram(program);
    }

    @GetMapping(params = "channelId")
    @ResponseStatus(HttpStatus.OK)
    public List<Program> getProgramsByChannel(@RequestParam(value="channelId") @NotBlank String channelId){
        return programService.getProgramsByChannel(channelId);
    }

    @GetMapping(params = "id")
    @ResponseStatus(HttpStatus.OK)
    public Program getProgram(@RequestParam(value= "id") @NotBlank String id){
        return programService.getProgram(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }


    @ApiOperation(value = "Delete a program that exist")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Channel deleted successfully"),
            @ApiResponse(code = 404, message = "Exception Program not found or dont exist"),
    })
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProgram(@PathVariable("id") @NotBlank String id){
        programService.getProgram(id).map(program -> {
            programService.deleteProgram(program.getId());
            return Void.TYPE;
        }).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Program Not Found!"));
    }


    @ApiOperation(value = "Update info of program that exist")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Channel updated successfully"),
            @ApiResponse(code = 404, message = "Exception Program not found or dont exist"),
    })
    @PutMapping("/{id}")
    public void updateProgram(@PathVariable("id") @NotBlank String id, @RequestBody Program program){
        programService.getProgram(id).map(programOriginal -> {
            modelMapper.map(program, programOriginal);
            programService.createProgram(programOriginal);
            /*programOriginal.setChannel(program.getChannel());
            programOriginal.setDescription(program.getDescription());
            programOriginal.setImageUrl(program.getImageUrl());
            programOriginal.setStartTime(program.getStartTime());
            programOriginal.setEndTime(program.getEndTime());
            programService.createProgram(programOriginal);*/
            return Void.TYPE;
        }).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Program Not Found!"));
    }

    // TODO Validation of inputs variables
    // TODO Exception handling

}
